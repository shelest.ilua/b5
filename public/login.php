<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

session_start(); //для доступа к переменным сессии

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации
//если есть сессия, то показываем кнопку выхода
if (!empty($_SESSION['login'])) {
    ?>
    <form method="POST">
        <input type="submit" name="sessiondestroy" value="Выход" />
    </form>
    <?php

}

//при нажатии кнопки выхода
if( isset( $_POST['sessiondestroy'] ) )
{
    session_destroy();
    header('Location: ./');
    exit();
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>
<form style="text-align: center" action="" method="post">
  <input name="login" />
  <input name="pwd" />
  <input type="submit" value="Войти" />
</form>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  $user = 'u35653';
  $pass = '4017880';
  $db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  //ищем такого пользователя и достаем его пароль
  try {
    $stmt = $db->prepare("SELECT pwd FROM userpassword WHERE login=:i");
    $result = $stmt->execute(array("i"=>$_POST['login']));
    $hashpwd = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $hashpwd = (current(current($hashpwd)));
  }
  catch(PDOException $e) {
      print('Error : ' . $e->getMessage());
      exit();
  }
  //если ничего не вытащили
  if (empty($hashpwd)){
    print ("Нет пользователя с такими данными");
  }
  else {
    if (password_verify($_POST['pwd'], $hashpwd)) {
        //если подходит пароль
      $_SESSION['uid'] = '12';
      $_SESSION['login'] = $_POST['login'];
      header('Location: ./');
      exit();
  // Делаем перенаправление.
  } else {
        //не подходит
      print( 'Пароль неправильный.');
  }
  }
  exit();
}
?>